﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using YetAnotherCMS.Entities;


namespace YetAnotherCMS.Services
{
    public interface IGenericRepository<T> where T: IGenericEntity
    {
        DynamoDBOperationConfig Config { get; }
        IDynamoDBContext Context { get; }
        Task<T> GetByIdAsyncsync(int id);
        Task SaveAsync(T item);
        Task DeleteByIdAsync( T item);
        Task<Dictionary<int, T>> FromScanAsync();
    }
}