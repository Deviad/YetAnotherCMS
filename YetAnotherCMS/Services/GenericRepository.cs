﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using YetAnotherCMS.Entities;

namespace YetAnotherCMS.Services
{
    public class GenericRepository<T> : IGenericRepository<T> where T : IGenericEntity
    {
        public DynamoDBOperationConfig Config { get; }
        public IDynamoDBContext Context { get; }

        public GenericRepository(DynamoDBOperationConfig config, IDynamoDBContext context)
        {
            Config = config;
            Context = context;
        }

        public async Task<T> GetByIdAsyncsync(int id)
        {
            try
            {
                var ID = Convert.ToInt32(id);
                return await Context.LoadAsync<T>(ID, Config);
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in Get operation! Error: {ex}");
            }
        }

        public async Task SaveAsync(T item)
        {
            try
            {
                await Context.SaveAsync(item, Config);
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in Write operation! Error: {ex}");
            }
        }

        public async Task DeleteByIdAsync(T item)
        {
            try
            {
                await Context.DeleteAsync(item);
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in Delete operation! Error: {ex}");
            }
        }

        public async Task<Dictionary<int, T>> FromScanAsync()
        {
            Dictionary<int, T> result = null;
            try
            {
                var search = Context.FromScanAsync<T>(new Amazon.DynamoDBv2.DocumentModel.ScanOperationConfig
                {
                    ConsistentRead = true
                }, Config);
                var searchResponse = await search.GetRemainingAsync();
                Console.WriteLine($"FromScanAsync");
                Console.WriteLine($"{searchResponse.ToString()}");
                if (searchResponse.Count > 0)
                {
                    result = searchResponse.Aggregate(new Dictionary<int, T> { }, (acc, elem) =>
                    {
                        acc.Add(((IGenericEntity) elem).Id, elem);
                        return acc;
                    });
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in GetAll operation! Error: {ex}");
            }
        }
    }
}