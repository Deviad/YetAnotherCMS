﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DocumentModel;
using Microsoft.IdentityModel.Tokens;
using YetAnotherCMS.Entities;

namespace YetAnotherCMS.Services
{
    public static class PostRepository
    {
        public static async Task<Dictionary<int, Post>> GetUserPosts(this GenericRepository<Post> postRepo,
            int userId)
        {
            Dictionary<int, Post> result = null;

            var context = postRepo.Context;

            var config = postRepo.Config;

            try
            {
                // var search = context.FromScanAsync<Post>(new Amazon.DynamoDBv2.DocumentModel.ScanOperationConfig
                // {
                //     ConsistentRead = true
                // }, config);
                // var searchResponse = await search.GetRemainingAsync();

                var search = context.FromQueryAsync<Post>(new Amazon.DynamoDBv2.DocumentModel.QueryOperationConfig()
                {
                    IndexName = "Posts-With-User-index",
                    Filter = new Amazon.DynamoDBv2.DocumentModel.QueryFilter("UserId",
                        Amazon.DynamoDBv2.DocumentModel.QueryOperator.Equal, userId)
                }, config);
                Console.WriteLine("items retrieved");
                var searchResponse = await search.GetRemainingAsync();
                // searchResponse.ForEach ((s) => {
                //     Console.WriteLine (s.ToString ());
                // });

                Console.WriteLine($"GetUserPosts");
                Console.WriteLine($"{searchResponse}");

                if (searchResponse.Count > 0)
                {
                    // We want to extract the user Id so we can conveniently filter the results, hence int is the userId.

                    result = searchResponse.Aggregate(new Dictionary<int, Post> { }, (acc, elem) =>
                    {
                        acc.Add(((IGenericEntity) elem).Id, elem);
                        return acc;
                    });
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in GetUserPosts operation! Error: {ex}");
            }
        }

        public static async Task<Dictionary<int, Post>> GetPostsByTitle(this GenericRepository<Post> postRepo, string title)
        {
            Dictionary<int, Post> result = null;

            var context = postRepo.Context;
            var config = postRepo.Config;
            ScanFilter scanFilter = new ScanFilter();
            scanFilter.AddCondition("Title", ScanOperator.Contains, title);
            try
            {
                var search = context.FromScanAsync<Post>(new Amazon.DynamoDBv2.DocumentModel.ScanOperationConfig()
                {
                    Filter = scanFilter
                }, config);
                var searchResponse = await search.GetRemainingAsync();
                
                
                Console.WriteLine($"getPostsByTitle");
                Console.WriteLine($"{searchResponse}");
                
                if (searchResponse.Count > 0)
                {
                    result = searchResponse.Aggregate(new Dictionary<int, Post> { }, (acc, elem) =>
                    {
                        acc.Add(((IGenericEntity) elem).Id, elem);
                        return acc;
                    });
                }    
                    
                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"Amazon error in getPostsByTitle operation! Error: {ex}");
            }
        } 
    }
}