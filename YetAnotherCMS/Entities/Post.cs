﻿using System.Collections.Generic;
using Amazon.DynamoDBv2.DataModel;

namespace YetAnotherCMS.Entities
{
    public class Post : IGenericEntity
    {
        [DynamoDBHashKey] public int Id { get; set; }
        [DynamoDBGlobalSecondaryIndexHashKey] public int UserId { get; set; }
        [DynamoDBGlobalSecondaryIndexHashKey] public string Title { get; set; }
        [DynamoDBProperty] public string Content { get; set; }
        [DynamoDBProperty] public string CreatedAt { get; set; }
        [DynamoDBProperty] public string UpdatedAt { get; set; }
        [DynamoDBProperty] public bool Trashed { get; set; }
    }
}