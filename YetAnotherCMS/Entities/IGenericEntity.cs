﻿using Amazon.DynamoDBv2.DataModel;

namespace YetAnotherCMS.Entities
{
    public interface IGenericEntity
    {
        [DynamoDBHashKey] int Id { get; set;}
    }
}