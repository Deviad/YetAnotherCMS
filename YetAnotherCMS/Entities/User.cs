﻿using System.Collections.Generic;
using Amazon.DynamoDBv2.DataModel;

namespace YetAnotherCMS.Entities
{    
    public class User:  IGenericEntity
    {
        [DynamoDBHashKey] public int Id { get; set; }
        [DynamoDBProperty] public string Name { get; set; }
        [DynamoDBProperty] public string Surname { get; set; }
        [DynamoDBProperty] public string Middlename { get; set; }
        [DynamoDBGlobalSecondaryIndexHashKey] public string Email { get; set; }
        [DynamoDBProperty] public string Address { get; set; }
        [DynamoDBProperty] public string Avatar { get; set; }
        [DynamoDBProperty] public string Password { get; set; }
        [DynamoDBProperty] public int Timezone { get; set; }
    }
}
