﻿using System.Collections.Generic;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using YetAnotherCMS.Data;
using YetAnotherCMS.Entities;
using YetAnotherCMS.Services;


namespace YetAnotherCMS
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());
            // AWS Options
            var awsOptions = Configuration.GetAWSOptions();
            services.AddDefaultAWSOptions(awsOptions);
            var serviceClient = awsOptions.CreateServiceClient<IAmazonDynamoDB>();
            var dbTables = new DynamoDbTables();
            ConfigurationBinder.Bind(Configuration.GetSection("DynamoDbTables"), dbTables);
            
            var dbServiceClient = new DynamoDBContext(serviceClient);
            var userDbOptions = new DynamoDBOperationConfig
            {
                OverrideTableName = dbTables.User
            };
            var postDbOptions = new DynamoDBOperationConfig
            {
                OverrideTableName = dbTables.Post
            };
            
            services.AddScoped<IGenericRepository<User>>(provider =>
                new GenericRepository<User>(userDbOptions, dbServiceClient));
            
            services.AddScoped<IGenericRepository<Post>>(provider =>
                new GenericRepository<Post>(postDbOptions, dbServiceClient));
  
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler();
            }

            app.UseStatusCodePages();
            
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Entities.User, Models.UserDto>();
                cfg.CreateMap<Entities.User, Models.UserWithPosts>();
                cfg.CreateMap<Entities.Post, Models.PostDto>();
            });
    
            app.UseMvc();
            
           
            

//            app.Run(async (context) => { await context.Response.WriteAsync("Hello World!"); });
        }
    }
}