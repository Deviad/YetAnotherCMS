﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Mvc;
using YetAnotherCMS.Data;
using YetAnotherCMS.Entities;
using YetAnotherCMS.Models;
using YetAnotherCMS.Services;
using YetAnotherCMS.Validators;

namespace YetAnotherCMS.Controllers
{
    [Route("/api/users")]
    public class UsersController : Controller
    {
        private IGenericRepository<User> _userRepository;
        private IGenericRepository<Post> _postRepository;

        public UsersController(IGenericRepository<User> userRepository, IGenericRepository<Post> postRepository)
        {
            _userRepository = userRepository;
            _postRepository = postRepository;
        }
        
        [HttpGet()]
        public async Task<IActionResult> GetAllUsers()
        {
            Dictionary<int, User> users = await _userRepository.FromScanAsync();
            if (users.Count == 0 )
            {
                return NotFound();
            }

            return Ok(users);
        }
        
        
        [HttpGet("{id}/withposts")]
        public async Task<IActionResult> GetAllUsersWithPosts(int id)
        {    
            
            User user = await _userRepository.GetByIdAsyncsync(id);
            
            Dictionary<int, Post> posts = await ((GenericRepository<Post>)_postRepository).GetUserPosts(id);
            
//            var userWithPosts = new UserWithPosts()
//            {
//                Id = user.Id,
//                Name = user.Name,
//                Surname = user.Surname,
//                Middlename = user.Middlename,
//                Email = user.Email,
//                Address = user.Address,
//                Avatar = user.Avatar,
//                UserPosts = posts
//            };
                     
            if (posts.Count == 0 )
            {
                return NotFound();
            }

            var userWithPosts = Mapper.Map<UserWithPosts>(user);
            userWithPosts.UserPosts = posts;                  
    
            return Ok(userWithPosts);
        }
        
        
        [HttpGet("{id}", Name = "GetUser")]
        public async Task<IActionResult> GetUserById(int id)
        {
            User user = await _userRepository.GetByIdAsyncsync(id);
            if (user == null)
            {
                return NotFound();
            }
                
            return Ok(user);
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreateUser(
            [FromBody][Validator(typeof(UserSignupValidator))]
            User user)
        {
            try
            {
                if (!ModelState.IsValid) //<----Validate here
                {
                    return new BadRequestObjectResult(ModelState);
                }

                await _userRepository.SaveAsync(user);
            }
            catch (Exception error)
            {
                BadRequest(error);  
            }
            return CreatedAtRoute(routeName: "GetUser",
                routeValues: new {id = user.Id},
                value: user);
        }
    }
}