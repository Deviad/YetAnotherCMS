﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using YetAnotherCMS.Entities;
using YetAnotherCMS.Services;
using YetAnotherCMS.Validators;

namespace YetAnotherCMS.Controllers
{
    [Route("/api/posts")]
    public class PostsController : Controller
    {
        private IGenericRepository<Post> _postRepository;

        public PostsController(IGenericRepository<Post> postRepository)
        {
            _postRepository = postRepository;
        }

        [HttpGet("{id}", Name = "GetPost")]
        public async Task<IActionResult> GetPostById(int id)
        {
            Post post = await _postRepository.GetByIdAsyncsync(id);

            if (post == null)
            {
                return NotFound();
            }

            return Ok(post);
        }
        
        [HttpGet("title/{title}")]
        public async Task<IActionResult> GetPostsByTitle(string title)
        {
            Dictionary<int, Post> posts = await ((GenericRepository<Post>)_postRepository).GetPostsByTitle(title);

            if (posts == null)
            {
                return NotFound();
            }

            return Ok(posts);
        }
        
        [HttpGet()]
        public async Task<IActionResult> GetAllPosts()
        {
            Dictionary<int, Post> posts = await _postRepository.FromScanAsync();
            if (posts.Count == 0)
            {
                return NotFound();
            }

            return Ok(posts);
        }

        [HttpPost("create")]
        public async Task<IActionResult> CreatePost(
            [FromBody] Post post)
        {
            try
            {
                if (!ModelState.IsValid) //<----Validate here
                {
                    return new BadRequestObjectResult(ModelState);
                }

                post.CreatedAt =  DateTime.UtcNow.ToString("u");
                await _postRepository.SaveAsync(post);
            }
            catch (Exception error)

            {
                BadRequest(error);
            }

            return CreatedAtRoute(routeName: "GetPost",
                routeValues: new {id = post.Id},
                value: post);
        }
    }
}