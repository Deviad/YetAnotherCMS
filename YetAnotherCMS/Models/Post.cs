﻿using System.Collections.Generic;
using Amazon.DynamoDBv2.DataModel;
namespace YetAnotherCMS.Models {
    public class PostDto {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int CreatedAt { get; set; }
        public int UpdatedAt { get; set; }
        public int DeletedAt { get; set; }
        public int TrashedAt { get; set; }
    }
}