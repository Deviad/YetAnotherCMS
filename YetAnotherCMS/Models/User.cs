﻿using System.Collections.Generic;
using Amazon.DynamoDBv2.DataModel;
using FluentValidation.Attributes;
using Microsoft.AspNetCore.Identity;
using YetAnotherCMS.Validators;

namespace YetAnotherCMS.Models
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Middlename { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Avatar { get; set; }
        public string Password { get; set; }
        public int Timezone { get; set; }
    }
}