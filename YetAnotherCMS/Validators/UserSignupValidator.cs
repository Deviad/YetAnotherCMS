﻿using FluentValidation;
using YetAnotherCMS.Entities;

namespace YetAnotherCMS.Validators
{
    public class UserSignupValidator : AbstractValidator<User>
    {
        public UserSignupValidator()
        {
            Initialize();
        }

        public void Initialize()
        {
//            When(x => x != null, () =>
            {
                RuleFor(_ => _.Name).NotEmpty().MaximumLength(50);
                RuleFor(_ => _.Surname).NotEmpty().MaximumLength(50);
                RuleFor(_ => _.Email).NotEmpty().MaximumLength(50).EmailAddress();
                RuleFor(_ => _.Address).NotEmpty().MaximumLength(100);
                //Password Validation
                // from 8 - to 16 chars
                // 1 lowercase required
                // 1 uppercase required
                // 1 special char required
                RuleFor(_ => _.Password).NotEmpty().MaximumLength(50).Matches(@"^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$");
                RuleFor(_ => _.Timezone).NotEmpty().NotEmpty().InclusiveBetween(0, 24);
            }
//                );
        }
    }
}