﻿using FluentValidation;
using YetAnotherCMS.Entities;

namespace YetAnotherCMS.Validators
{
    public class CreatePostValidator: AbstractValidator<Post>
    {

        public CreatePostValidator()
        {
            Initialize();
        }
        
        public void Initialize()
        {
            {
                RuleFor(_ => _.UserId).NotEmpty();
                RuleFor(_ => _.Title).NotEmpty().MaximumLength(150);
                RuleFor(_ => _.Content).NotEmpty();
            }
        }
    }
}